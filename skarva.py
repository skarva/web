import sys


from flask import Flask, render_template
from flask_flatpages import FlatPages
from flask_frozen import Freezer


FLATPAGES_AUTO_RELOAD = False # For debugging only!
FLATPAGES_EXTENSION = '.md'


app = Flask(__name__)
app.config.from_object(__name__)
pages = FlatPages(app)
freezer = Freezer(app)


@app.route('/')
def news():
    # Articles are pages with a publication date
    articles = (p for p in pages if 'published' in p.meta)
    # Show the 10 most recent articles, most recent first.
    latest = sorted(articles, reverse=True,
        key=lambda p: p.meta['published'])
    return render_template('articles.html', articles=latest[:10])


@app.route('/about/')
def about():
    return render_template('about.html')


@app.route('/games/')
def games():
    releases = (p for p in pages if 'released' in p.meta)
    return render_template('games.html', games=releases)


@app.route('/apps/')
def apps():
    releases = (p for p in pages if 'released' in p.meta)
    return render_template('apps.html', apps=releases)


@app.route('/<path:path>/')
def load_path(path):
    page = pages.get_or_404(path)
    return render_template('article_template.html', page=page)


if __name__ == '__main__':
    if len(sys.argv) > 1 and sys.argv[1] == 'build':
        freezer.freeze()
    else:
        app.run(port=5000)

