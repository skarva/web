title: Adventure Galore on Linux
subtitle: Bacon Man and Zed join the Linux gaming community
author: Ryan
published: 2019-09-30
tags: [game]

Catching up on releases we've done and I am happy to spread the word about two indie games that have been ported to Linux; Bacon Man and Zed!

# Bacon Man: An Adventure
With the world resting on his shoulders, take on the role of Bacon Man. Battle through the food groups and confront those who condemned you. It’s up to you to clear his name, by murdering those who put him behind bars.

Coming Soon!

[Find out more here](https://baconmangame.com)

# Zed
From the mind of Chuck Carter (Myst, Command and Conquer), ZED is an explorative adventure into whimsical dreamworlds. This beautiful and meditative game encourages curiosity and imagination. The Dreamer needs your help – there are mysteries to solve and memories to unlock. Eagre Games has long been immersed in these dreamscapes and cannot wait to share them with the world.

Available now on [Steam](https://store.steampowered.com/app/953370/ZED/) and [GOG](https://www.gog.com/game/zed)

[Find out more here](https://eagregames.com/)
