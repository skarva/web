title: Lock Box
type: app
released: Aug 10 2019
logo: lockbox.png

Lock Box is a simple password and secret note manager for Elementary OS. Keep your notes and website credentials secure in an easy-to-manage collection. It is ready to go when you're logged in, and securely encrypted when you're not. All with the elegent UI you expect from apps on eOS.

<a href="https://appcenter.elementary.io/com.github.skarva.lockbox"><img src="https://appcenter.elementary.io/badge.svg" alt="Get it on AppCenter" /></a>

[View Source on Github](https://github.com/skarva/lockbox/)
