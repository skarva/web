title: "Bacon Man: An Adventure"
type: port
released: Coming Soon
logo: bman-logo.png

Bringing the fight for the Food Kingdoms to Linux was a blast, particularly because of the game having couch cooperative. While it wasn't ported in time for the initial release, it did get to bake a little longer and get all the fixes and enhancements of the Well Done Edition.

[Find out more about Bacon Man here](https://baconmangame.com)
