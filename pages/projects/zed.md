title: Zed
type: port
released: June 2019
logo: zed-logo.png

Help reconnect fading memories in Linux! This native port brings both the standard and VR experience from Myst artist Chuck Carter to the Linux Desktop. It was a real privilege to meet Chuck and work with Cyan to build and test the port.

[Find out more about Zed here](https://eagregames.com/)
