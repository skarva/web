# Skarva website

### Dependencies
Make a new virtualenv and then use pip to install:

* Flask
* Flask-Flatpages
* Frozen-Flask

### Testing
To test set up the environment variables

    export FLASK_APP=skarva.py
    export FLASK_ENV=development

Then start the dev server with `flask run` which will host on `localhost:5000`

### Building and Deploying
To build simply call `python skarva.py build` which will run Frozen-Flask and
generate a `build` folder. The contents of this folder is what you want to
deploy to your webserver of choice. Easy!
